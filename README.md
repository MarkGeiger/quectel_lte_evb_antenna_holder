# Quectel_LTE_EVB_Antenna_holder

This repo contains the raw blender files and the .stl for an antenna holder, designed for the Quectel LTE Open EVB kit:
https://www.quectel.com/product/lteopenkit.htm


New Version in Blender:

![Blender Screenshot](./pics/blender.png)

## Missing Wifi Antenna

The following photo shows an older version of this holder without an additional socket for the Wifi-Module. The newer version already supports the additional third antenna:


Old version:

![EVB](./pics/evb.png)

## Slicing

The model is designed to be printed laying on the backside. Print with supports. However, almost no supports are needed. 

![Cura Screenshot](./pics/cura.png)
